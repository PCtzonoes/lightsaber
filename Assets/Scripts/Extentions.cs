using UnityEngine;

public static class Extentions
{
    public static Vector3 CameraDirection(this Vector3 direction, Transform mainCamera)
    {
        var cameraRight = mainCamera.transform.right;
        cameraRight.y = 0f;

        var cameraForward = Vector3.Cross(cameraRight, Vector3.up);
        
        return cameraForward * direction.z + cameraRight * direction.x;
    }
}
