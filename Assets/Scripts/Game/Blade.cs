using System;
using Helper;
using UnityEngine;


public class Blade : MonoBehaviour
{
	[SerializeField] private GameEvent onHit;
	[SerializeField] private GameEvent OnMiss;
	[SerializeField] private Hittype type = Hittype.Blue;

	private void OnCollisionEnter(Collision collision)
	{
#if UNITY_EDITOR
		Debug.Log("Colliding");
#endif

		var cube = collision.gameObject.GetComponent<Cubes>();

		if (cube == null) return;

		if (cube.Type != type)
		{
			OnMiss.Invoke();
			Destroy(cube.gameObject);
			return;
		}
		
		var point = collision.GetContact(0).point;

		if (cube.CheckDistance(point)) onHit.Invoke();
		else OnMiss.Invoke();

#if UNITY_EDITOR
		Debug.Log("SucessHit");
#endif

		Destroy(cube.gameObject);
	}
}