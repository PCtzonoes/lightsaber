using System;
using System.Collections;
using System.Collections.Generic;
using Helper;
using UnityEngine;

public class Cubes : MonoBehaviour
{
	[SerializeField] private GameEvent onMiss;
	[SerializeField] private Hittype type = Hittype.Blue;
	[SerializeField] private Transform rightHit;
	[SerializeField] private Transform wrongHit;
	[SerializeField] private float speed = 3;

	public Hittype Type => type;

	private void Update()
	{
		transform.position += transform.forward * speed * Time.deltaTime;

		if (transform.position.z > 18)
		{
			onMiss.Invoke();
			Destroy(gameObject);
		}
	}

	public bool CheckDistance(Vector3 point)
	{
		var distRight = Vector3.Distance(point, rightHit.position);
		var distWrong = Vector3.Distance(point, wrongHit.position);

		return distRight <= distWrong;
	}
}