using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class Spawner : MonoBehaviour
{
	[SerializeField] private GameObject[] cubes;
	[SerializeField] private Vector2 maxOffset;

	private bool _changeCube;
	private float _timer = 2;

	private void Start()
	{
		if (cubes == null)
		{
			Debug.LogError("Missing cubes in spawnerPrefab", this);
			gameObject.SetActive(false);
		}
	}

	public void SpawnCubes()
	{
		//Random startPoint
		var x = Random.Range(-maxOffset.x, maxOffset.x);
		var y = Random.Range(-maxOffset.y, maxOffset.y);
		y += transform.position.y;
		var spawnPoint = new Vector3(x, y, transform.position.z);

		var index = Random.Range(0, 999);

		var go = Instantiate(cubes[index % 2], spawnPoint, Quaternion.identity);
		var rotation = new Vector3(0, 0, 90 * (index % 4));
		go.transform.Rotate(rotation);
	}

	private void Update()
	{
		_timer -= Time.deltaTime;

		if (_timer < 0)
		{
			_timer = 2;
			SpawnCubes();
		}
	}
}