using System.Collections.Generic;
using UnityEngine;

namespace Helper
{
	[CreateAssetMenu(menuName = "GameEvent/Default")]
	public class GameEvent : ScriptableObject
	{
		private readonly HashSet<GameEventListener> _listeners = new();

		public void Invoke()
		{
			foreach (var eventListener in _listeners)
			{
				eventListener.RaiseEvent();
			}
		}

		public void Register(GameEventListener gameEventListener) => _listeners.Add(gameEventListener);
		public void Remove(GameEventListener gameEventListener) => _listeners.Remove(gameEventListener);
	}
}
