using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Helper
{
	public class GameEventListener : MonoBehaviour
	{
		[SerializeField] protected GameEvent gameEvent;
		[SerializeField] protected UnityEvent unityEvent;

		private void OnEnable()
		{
			OnEnableGameEvent();
		}
 
		private void OnDisable()
		{
			OnDisableGameEvent();
		}

		protected virtual void OnEnableGameEvent()
		{
			gameEvent.Register(this);
		}

		protected virtual void OnDisableGameEvent()
		{
			gameEvent.Remove(this);
		}

		public virtual void RaiseEvent()
		{
			unityEvent.Invoke();
		}
	}
}