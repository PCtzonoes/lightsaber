using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] int levelToLoadNumber = 1;
    [SerializeField] bool toLoadOnStartup = true;
    void Start()
    {
        if (toLoadOnStartup)
            SceneManager.LoadScene(levelToLoadNumber, LoadSceneMode.Single);
    }
}
