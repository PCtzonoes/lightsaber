using System;
using Helper;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


	/// <summary>
	/// this Ui Script is been used for Score Saving
	/// </summary>
	public class GameplayUIScreen : MonoBehaviour
	{
		[SerializeField] private TMP_Text score;
		[SerializeField] private TMP_Text combo;

		private float _score = 0;
		private int _comboPoints;

		public void BoxHit()
		{
			_score += 5 + _comboPoints * 2;
			_comboPoints++;
			score.text = $"Score: {_score}";
			combo.text = $"Combo: {_comboPoints}";
		}

		public void BoxMiss()
		{
			_comboPoints = 0;
			combo.text = $"Combo: {_comboPoints}";
		}
	}
